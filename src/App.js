import React, { Component } from 'react';
import { Provider } from "mobx-react";
import StockStore from "./store/StockStore.js"
import AddStock from "./components/AddStock.js"
import StockList from "./containers/StockList.js"

const stores = new StockStore();

class App extends Component {
  render() {
    return (
      <Provider stocks={stores}>
        <div>
          <AddStock />
          <StockList />
        </div>
      </Provider>
    );
  }
}

export default App;
