import React from "react";
import { inject, observer } from "mobx-react";

@inject("stocks")
@observer
export default class AddStock extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeFile = this.handleChangeFile.bind(this);
  }

  handleClick(event) {
    event.preventDefault();
    const { stocks } = this.props;
    if (stocks.inputItem.itemName && stocks.inputItem.itemName.length > 0) {
      stocks.addItem(stocks.inputItem);
    }
  }

  handleChange(event) {
    const { stocks } = this.props;
    const newitem = event.target && event.target.value;
    stocks.inputItem.itemName = newitem.trim();
  }

  handleChangeFile(event) {
    const { stocks } = this.props;
    stocks.inputItem.file = event.target.files[0];
    stocks.inputItem.previewSrc = URL.createObjectURL(stocks.inputItem.file);
  }

  render() {
    const { stocks } = this.props;
    return (
      <div>
        <input type="text" onChange={this.handleChange} value={stocks.inputItem.itemName} />
        &nbsp;
        <input type="file" accept="image/*" onChange={this.handleChangeFile} />
        &nbsp;
        {stocks.inputItem.previewSrc && <img src={stocks.inputItem.previewSrc} height="50" />}
        &nbsp;
        <button onClick={this.handleClick}>登録</button>
      </div>
    );
  }
}
