import React from "react";
import { inject, observer } from "mobx-react";

@inject("stocks")
@observer
export default class ChangeQuantity extends React.Component {
  render() {
    const stocks = this.props.stocks;
    return (
      <span>
        <button onClick={() => stocks.increaseItemById(this.props.item.id)}>+</button>
        &nbsp;
        <button onClick={() => stocks.decreaseItemById(this.props.item.id)}>-</button>
        &nbsp;
        <strong>{this.props.item.quantity} 個</strong>
      </span>
    );
  }
}
