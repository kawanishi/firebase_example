import React from "react";
import { inject, observer } from "mobx-react";

@inject("stocks")
@observer
export default class DeleteStock extends React.Component {
  render() {
    const stocks = this.props.stocks;
    return (
      <span>
        <button onClick={() => stocks.removeItemById(this.props.item.id)}>削除</button>
      </span>
    );
  }
}
