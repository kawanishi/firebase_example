import React from "react";
import { inject, observer } from "mobx-react";
import ChangeQuantity from "../components/ChangeQuantity.js"
import DeleteStock from "../components/DeleteStock.js"

@inject("stocks")
@observer
export default class StockList extends React.Component {
  componentWillMount() {
    const stocks = this.props.stocks;
    stocks.refreshStockList();
  }

  render() {
    const stocks = this.props.stocks;
    return (
      <div>
        {stocks.items.map(item => (
          <div>
            {item.name} <img src={item.imageUrl} height="50" /> <ChangeQuantity item={item} /> <DeleteStock item={item} />
            </div>
        ))}
      </div>
    )
  }
}
