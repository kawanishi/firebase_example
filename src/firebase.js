import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyC8Er4s-xcz6TWz6cV_26aE08vv_N1khQI",
  authDomain: "fir-example-eceed.firebaseapp.com",
  databaseURL: "https://fir-example-eceed.firebaseio.com",
  projectId: "fir-example-eceed",
  storageBucket: "fir-example-eceed.appspot.com",
  messagingSenderId: "517134370266"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
export const firebaseDb = firebaseApp.database();
export const firebaseStorage = firebaseApp.storage();
