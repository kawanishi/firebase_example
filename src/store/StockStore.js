import { observable } from "mobx";
import { firebaseDb } from "../firebase.js";
import { firebaseStorage } from "../firebase.js";

export default class StockStore {
  @observable items = [];

  @observable
  inputItem = {
    itemName: "",
    file: [],
    previewSrc: ""
  }

  refreshStockList() {
    this.items.clear();
    firebaseDb.ref("items").on("child_added", (snapshot) => {
      if (snapshot.val().delFlg === false) {
        this.items.push({
          id: snapshot.val().id,
          name: snapshot.val().name,
          imageUrl: snapshot.val().imageUrl,
          quantity: snapshot.val().quantity,
          delFlg: snapshot.val().delFlg
        });
      }
    });
  }

  addItem(postItem) {
    firebaseDb.ref("items").orderByChild("id").limitToLast(1).once("value").then(snapshot => {
      const max = snapshot.val();
      let maxId = 0;
      for (let key in max) {
        if (max[key].id > 0) {
          maxId = max[key].id;
        }
      }
      firebaseStorage.ref().child(postItem.itemName).put(postItem.file).then(snapshot => {
        const url = snapshot.downloadURL;
        maxId += 1;
        firebaseDb.ref("items").push({
          id: maxId,
          name: postItem.itemName,
          imageUrl: url,
          quantity: 0,
          delFlg: false
        });
        this.inputItem = {
          itemName: "",
          file: [],
          previewSrc: ""
        }
      });
    });
  }

  increaseItemById(id) {
    firebaseDb.ref("items").once("value").then(snapshot => {
      const tmpItems = snapshot.val();
      const updates = {};
      for (let key in tmpItems) {
        if (tmpItems[key].id === id) {
          updates["/" + key] = {
            id: tmpItems[key].id,
            name: tmpItems[key].name,
            imageUrl: tmpItems[key].imageUrl,
            quantity: tmpItems[key].quantity + 1,
            delFlg: tmpItems[key].delFlg
          }
          firebaseDb.ref("items").update(updates);
        }
      }
      this.refreshStockList();
    })
  }

  decreaseItemById(id) {
    firebaseDb.ref("items").once("value").then(snapshot => {
      const tmpItems = snapshot.val();
      const updates = {};
      for (let key in tmpItems) {
        if (tmpItems[key].id === id && tmpItems[key].quantity > 0) {
          updates["/" + key] = {
            id: tmpItems[key].id,
            name: tmpItems[key].name,
            imageUrl: tmpItems[key].imageUrl,
            quantity: tmpItems[key].quantity - 1,
            delFlg: tmpItems[key].delFlg
          }
          firebaseDb.ref("items").update(updates);
        }
      }
      this.refreshStockList();
    });
  }

  removeItemById(id) {
    firebaseDb.ref("items").once("value").then(snapshot => {
      const tmpItems = snapshot.val();
      const updates = {};
      for (let key in tmpItems) {
        if (tmpItems[key].id === id) {
          updates["/" + key] = {
            id: tmpItems[key].id,
            name: tmpItems[key].name,
            imageUrl: tmpItems[key].imageUrl,
            quantity: tmpItems[key].quantity,
            delFlg: true
          }
          firebaseDb.ref("items").update(updates);
        }
      }
      this.refreshStockList();
    });
  }
}
